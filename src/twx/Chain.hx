package twx;

import hx.Dispatcher;

import stx.Prelude;
import stx.Bools;

import stx.reactive.Streams;
import stx.reactive.Reactive;

using stx.Arrays;
using stx.Functions;
using stx.Options;

using twx.Chain;

class Task<T> extends Dispatcher<Async<T>>{
  private function propagator (x:Pulse<Dynamic>):Propagation<Dynamic> return failed ? doNotPropagate : propagate(x)
  public function new(composite = false){
    super();
    this.id = Chain.id++;this.failed = false;
    if (composite) return;

    this.stream = 
      Streams.create( propagator )
      .map( Chain.identify.p1(id) );

    this.stream.foreach(
      function(x){
        switch (x.response) {
          case Progress(v)  : dispatch( Progress(v) );
          case Done(b)      : dispatch( Done(b) );
          case Error(e)     : failed = true; dispatch( Error(e) );
          default           :
        }
      }
    );
  }
  public function run(){ 
    dispatch( Start );
  }
  public function send(v:Async<Dynamic>){ stream.sendEvent(v); }
  private var failed  : Bool;
  public var id       : Int;
  public var stream(default,null): Stream<AsyncAndId<Dynamic>>;
}
class SequentialTask<A,B> extends Task<Dynamic>{
  var t1 : Task<A>;
  var t2 : Task<B>;
  var o1 : Dynamic;
  var o2 : Dynamic;
  public function new(t1,t2){
    this.t1 = t1;this.t2 = t2;super( true );
    this.stream = new Stream( propagator, [t1.stream,t2.stream] )
      .map(
        function(x:AsyncAndId<Dynamic>):Async<Dynamic>{
          return switch (x.id) {
            case    t1.id :
              switch (x.response) {
                case Progress(i)  : Progress( i / 2 );
                case Done(v)  : o1 = v; t2.run(); x.response;
                default           : x.response;
              }
            case    t2.id :
              switch (x.response) {
                case Progress(i)  : Progress( .5 + (i / 2) );
                case Done(v)  : 
                o2 = v; 
                var val = [o1].append(o2);
                var evt = Done(val);
                dispatch(evt);
                Done(val);
                default           : x.response;
              }
          }
        }
      ).map( Chain.identify.p1(id) )
       .foreach(
        function(x:AsyncAndId<Dynamic>):Void{
          switch (x.response) {
            case Error(e)     : failed = true;      dispatch(Error(e));
            case Progress(i)  :                     dispatch(Progress(i));
            default :
          }
        }
      );
  }
}
class ParallelTask<A,B> extends Task<Dynamic>{
  var t1        : Task<A>;
  var t2        : Task<B>;
  var o1        : Option<Dynamic>;
  var o2        : Option<Dynamic>;
  var progress  : Array<Float>;

  public function new(t1,t2){
    this.t1 = t1;this.t2 = t2;this.progress = [0.,0.];o1=None;o2=None;
    super( true );
    this.stream 
      = Streams.create( propagator , [t1.stream,t2.stream] )
      .map(
        function(x){
          return switch (x.response) {
            case Error(e) : this.failed = true; dispatch(Error(e) );x.response;
            case Done(v)  : 
              switch (x.id) {
                case t1.id    : o1 = Some(v);
                case t2.id    : o2 = Some(v);
              }
              o1.zip(o2).foreach( function(x) dispatch(Done(x.elements() )) );x.response;
            case Progress(v)  : 
              switch (x.id) {
                case t1.id    : progress[0] = v;
                case t2.id    : progress[1] = v;
              }
              trace (progress);
              var p = (progress[0]+progress[1])/2;
              dispatch( Progress(p));
              Progress(p);
            case Start        : Start;
          }
        } 
      ).map( Chain.identify.p1(id) );
  }
  override public function run(){
    t1.run.thenDo(t2.run)();
  }
}
class Chain{
  public static function identify(id:Int,a:Async<Dynamic>){
    return { id : id , response : a };
  }
  public static var id : Int;
  static function __init__() id = 0
  public static function then(t1:Task<Dynamic>,t2:Task<Dynamic>):Task<Dynamic>{
    return new SequentialTask(t1,t2);
  }
  public static function and(t1:Task<Dynamic>,t2:Task<Dynamic>):Task<Dynamic>{
    return new ParallelTask(t1,t2);
  }
}
enum Async<T>{
  Start;
  Progress(v:Float);
  Done(v:T);
  Error(v:Dynamic);
}
class Events{
  static public function onStart(f:Void->Void){
    return 
      function(x){ 
        switch (x) {
          case Start  : f();
          default     :
        }
      }
  }
  static public function onProgress<A>(f:Float->Void){
    return
      function(x:Async<A>){
        switch (x) {
          case Progress(v) : f(v);
          default     :
        }
      }
  }
  static public function onDone<A>(f:A->Void){
    return 
      function(x){
        switch (x) {
          case Done(v) : f(v);
          default     :
        }
      }
  }
  static public function onError(f:Dynamic->Void){
    return 
      function(x){
        switch (x) {
          case Error(v) : f(v);
          default     :
        }
      }
  }
}
typedef AsyncAndId<T> = { id : Int, response : Async<T> };
using twx.Chain;

using Std;

using Main;
import stx.Prelude;

using stx.Tuples;
using stx.Iterables;
using stx.Functions;
using stx.Options;
using stx.reactive.Streams;
using stx.reactive.Reactive;

#if flash
import flash.events.AsyncErrorEvent;
import flash.events.Event;
import flash.events.ProgressEvent;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.display.Loader;
import flash.net.URLRequest;
#end
import stx.reactive.Arrows;     using stx.reactive.Arrows;

import hx.Dispatcher;

using stx.Enums;

import stx.reactive.Reactive;
class Main{
  static function main(){

    var a       = new Loader();

    var req_a   = new URLRequest('assets/Roberta Daar Wall Wood Sculpture Abstract Decorative Art.jpg');

    //Make lazy so it doesn't start until the task is started.
    var a_starter = a.load.lazy(req_a,null);

    var a_task = Tasks.loader(a);
        //run starter when task is started
        a_task.add( Events.onStart(a_starter) );

    //Print when done.
    a_task.add(Events.onDone(function(x) trace('A Done $x'.format())));

    var b     = new Loader();
    var req_b = new URLRequest('assets/29848312.jpg');

    //Make lazy so it doesn't start until the task is started.
    var b_starter = b.load.lazy(req_b,null);

    var b_task = Tasks.loader(b);
        //run starter when task is started
        b_task.add( Events.onStart(b_starter) );

    //print A progress
    a_task.add( Events.onProgress(function(x)         trace('A progress $x'.format())));
    //print B progress
    b_task.add( Events.onProgress(function(x)         trace('B progress $x'.format())));
    
    //Do these tasks concurrently (flash won't load concurrently, but they are triggered at the same time)
    var a_and_b = a_task.and(b_task);

    //Print the aggregate progress
    a_and_b.add( Events.onProgress(function(x)        trace('A and B progress $x'.format())));
    //Print aggregate Done
    a_and_b.add( Events.onDone( untyped function(x)   trace('A and B Done $x'.format())));
    //Print aggregate Error
    a_and_b.add( Events.onError( untyped function(x)  trace('A and B Error $x'.format())));

    //Start her up.
    a_and_b.run();
  }
}
class Tasks{
  public static function loader(loader:Loader){
    var task    = new Task(false);

    loader.contentLoaderInfo.addEventListener(Event.COMPLETE,
      function(x){
        task.send( Done(loader) );
      }
    );
    loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS,
        function(e:ProgressEvent){
          task.send( Progress( e.bytesLoaded / e.bytesTotal ) );
        }
    );
    loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,
        function(e:IOErrorEvent){
          task.send( Error(e) );
        }
    );
    loader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR,
        function(e:SecurityErrorEvent){
          task.send( Error(e) );
        }
    );
    loader.contentLoaderInfo.addEventListener(AsyncErrorEvent.ASYNC_ERROR,
        function(e:AsyncErrorEvent){
          task.send( Error(e) );
        }
    );
    return task;
  }
}
